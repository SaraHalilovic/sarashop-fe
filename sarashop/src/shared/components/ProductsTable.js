import React from 'react'

export default function ProductsTable(props) {

    const columns = props.columns.map(column => <th key={column} scope="col">{column}</th>)
    return (
        <table className="table cartList">
            <thead>
                <tr>
                    {columns}
                </tr>
            </thead>
            <tbody>
                {props.productList}
            </tbody>
        </table>
    )
}