import React from "react";
import { Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

export default function CartItemCard(props) {


    return (
        <tr>
            <th scope="row"><img className="cart-item--img" src={props.image} alt="Image not found!" /></th>
            <Link className="cart--product-details-link" to={`/product/${props.id}`}>
                <td>{props.name}</td>
            </Link>
            <td>${props.price}</td>
            <td>${props.shippingPrice}</td>
            <td>{props.quantity}</td>
            <td><button onClick={() => props.handleDelete(props.id)}> Delete</button></td>
            <td>${props.quantity * props.price + props.shippingPrice}</td>
        </tr>

    )

}