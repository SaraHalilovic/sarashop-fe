import React from 'react';

function ProductFilterPanel(props) {
    /*ovdje ce mi trebati state, dodati name and value na sve fieldove i onChange*/
    const [formData, setFormData] = React.useState(
        {
            keyword: "",
            category: "",
            priceFrom: 0,
            priceTo: 0,
            freeShipping: false,
            colour: "",
            condition: ""
        }
    )

    function handleChange(event) {
        const { name, value, type, checked } = event.target
        setFormData(prevFormData => {
            return {
                ...prevFormData,
                [name]: type === "checkbox" ? checked : value
            }
        })
    }

    function reset() {
        props.handleReset();
        setFormData({
            keyword: "",
            category: "",
            priceFrom: 0,
            priceTo: 0,
            freeShipping: false,
            colour: "",
            condition: ""
        })
    }

    function submitaj(event) {
        event.preventDefault()
        props.handleSubmit(formData)
    }
    return (
        <div className="product-filter-panel bg-dark">
            <form onSubmit={submitaj}>

                <hr className="separator"></hr>
                <div className="product-filter-panel--header-section">
                    <h5 id="product-filter-panel--header">Filter</h5>
                    <div class="btn-group filter-buttons" role="group">
                        <button type="reset" onClick={reset} >Reset</button>
                        <button >Apply</button>
                    </div>
                </div>

                <hr className="separator"></hr>

                <fieldset className="product-filter-panel--filter">
                    <input
                        id="product-filter-panel--search-keyword"
                        name="keyword"
                        placeholder="Search"
                        onChange={handleChange} />
                    <br />
                </fieldset>

                <fieldset className="product-filter-panel--filter">
                    <label className="product-filter-panel--label">Price Range</label>
                    <div id="price-filter">
                        <input
                            className="product-filter-panel--price"
                            id="product-filter-panel--priceFrom"
                            name="priceFrom"
                            placeholder="From"
                            onChange={handleChange}
                        />

                        <input
                            className="product-filter-panel--price"
                            id="product-filter-panel--priceTo"
                            placeholder="To"
                            name="priceTo"
                            onChange={handleChange}
                        />
                    </div>
                </fieldset>

                <fieldset className="product-filter-panel--filter">
                    <label className="product-filter-panel--label">Category</label>
                    <select
                        id="product--category product-input"
                        value={formData.category}
                        onChange={handleChange}
                        name="category"
                        className="product-input"
                    >
                        <option value=""></option>
                        <option value="toys">Toys</option>
                        <option value="equipment">Equipment</option>
                        <option value="clothes">Clothes</option>
                    </select>
                    <br />
                </fieldset>

                <fieldset className="product-filter-panel--filter">
                    <label className="product-filter-panel--label">Colour</label>
                    <select
                        id="product--colour"
                        className="product-input"
                        value={formData.colour}
                        onChange={handleChange}
                        name="colour"
                    >
                        <option value=""></option>
                        <option value="Black">Black</option>
                        <option value="blue">Blue</option>
                        <option value="Green">Green</option>
                        <option value="Purple">Purple</option>
                    </select>
                    <br />
                </fieldset>

                <fieldset className="product-filter-panel--filter">
                    <legend>Condition</legend>
                    <input
                        type="radio"
                        id="new"
                        name="condition"
                        value="new"
                        onChange={handleChange}
                        checked={formData.condition === "new"}
                    />
                    <label htmlFor="new">New</label>
                    <br />

                    <input
                        type="radio"
                        id="used"
                        name="condition"
                        value="used"
                        onChange={handleChange}
                        checked={formData.condition === "used"}
                    />
                    <label htmlFor="used">Used</label>
                    <br />
                </fieldset>

                <input
                    type="checkbox"
                    id="freeShipping"
                    checked={formData.freeShipping}
                    onChange={handleChange}
                    name="freeShipping"
                />
                <label htmlFor="freeShipping">Free Shipping?</label>
                <br />
            </form>
        </div>
    )
}

export default React.memo(ProductFilterPanel)