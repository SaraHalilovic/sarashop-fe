import React from 'react'
import OrderDetailsModal from './OrderDetailsModal'

export default function OrderCard(props) {

    const [show, setShow] = React.useState(false)

    return (
        <div className="card order-card">
            <div className="card-body">
                <h5 className="card-title">Order ID : {props.id} </h5>
                <p className="card-text">Date: {new Date(props.date).toLocaleString()}</p>
                <p className="card-text">Total: ${props.total}</p>
                <button className="btn btn-dark"
                    onClick={() => setShow(true)}>
                    Order details
                </button>
                <OrderDetailsModal
                    onClose={() => setShow(false)}
                    show={show}
                    id ={props.id} />
            </div>
        </div>
    )
}