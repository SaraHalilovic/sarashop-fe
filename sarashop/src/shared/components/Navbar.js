import { Link } from 'react-router-dom'

export default function Navbar() {
    return (
        <nav className="navbar navbar-dark sticky-top bg-dark">
            <Link to="/">
                <img className="nav--logo" src="https://th.bing.com/th/id/R.7ea9f72068b640b007dfd7cab83bfb30?rik=q4NLI7Qn%2fHsq7A&pid=ImgRaw&r=0" />
            </Link>
            <div className="main-nav-links">
                <Link className="main-nav-link" to="/">Home</Link>
                <Link className="main-nav-link" to="/cart">Cart</Link>
                <Link className="main-nav-link" to="/history">My Orders</Link>
            </div>
        </nav>
    )
}