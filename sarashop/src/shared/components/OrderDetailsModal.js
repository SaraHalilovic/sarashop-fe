import React from 'react'
import { useQuery } from 'react-query'
import OrderProductDetailsCard from './OrderProductDetailsCard'
import ProductsTable from './ProductsTable'

export default function OrderDetailsModal(props) {

    if (!props.show) {
        return null
    }

    const fetchOrderHistory = async ({ queryKey }) => {
        const response = fetch(`https://localhost:44343/order/${queryKey[1]}`).then(res => res.json())
        return response;
    }

    const { data, status, refetch } = useQuery(["orderHistory", props.id], fetchOrderHistory, {
       // refetchOnWindowFocus: false,
        //enabled: false // turned off by default, manual refetch is needed
    });

    if (status === "loading")
        return <div>Loading ...</div>

    if (status === "error")
        return <div>Error!</div>

    const columns = ["Product", "Name", "Price", "Shipping Price", "Quantity"]
    
    const orderList = data.orderProduct.map((item) =>
        <OrderProductDetailsCard
            key={item.product.productId}
            id={item.product.productId}
            name={item.product.name}
            image={item.product.image}
            quantity={item.quantity}
            price={item.product.price}
            shippingPrice={item.product.price}/>)

    return (
        <div className="order-details-modal" onClick={props.onClose}>
            <div className="modal-content-order" onClick={e => e.stopPropagation()}>
                <div className="modal-header-order">
                    <h5 className="modal-title-order">Order details</h5>
                    <div className="modal-body-order">
                        <ProductsTable productList={orderList} columns={columns}/>
                        <h6 className = "order-details-modal--total-price">Total: ${data.totalPrice}</h6>
                    </div>
                    <div className="modal-footer-order">
                        <button type="button"
                            className="btn btn-secondary order-details-modal--close-modal-btn"
                            onClick={props.onClose}>Close</button>
                    </div>
                </div>
            </div>
        </div>
    )

}