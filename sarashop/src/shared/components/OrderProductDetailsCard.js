import React from "react";
import { Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

export default function OrderProductDetailsCard(props) {


    return (
        <tr>
            <th scope="row"><img className="cart-item--img" src={props.image} alt="Image not found!" /></th>
            <td>{props.name}</td>
            <td>${props.price}</td>
            <td>${props.shippingPrice}</td>
            <td>{props.quantity}</td>
        </tr>

    )

}