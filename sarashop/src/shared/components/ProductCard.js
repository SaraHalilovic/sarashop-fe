import React from 'react'
import { Link } from 'react-router-dom'

export default function ProductCard(props){
    return(
        <div className="product-card-container bg-light">
            <img className="product-card-image" src={props.image} alt="Error"/>
            <h4 className="product-card-title">{props.name}</h4>
            <p className="product-card-description">{props.shortDescription}</p>
            <h4 className="product-card-price">${props.price}</h4>
            <Link to={`/product/${props.id}`}>
            <button className="product-card-buynow-btn bg-secondary">Buy Now</button>
            </Link>
        </div>
    )
}