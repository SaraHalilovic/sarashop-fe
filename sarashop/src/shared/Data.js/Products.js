export default [
    {
        "productId": 2,
        "code": "123568988882",
        "isActive": 1,
        "name": "Pokemon Plush       ",
        "category": "Toys      ",
        "image": "https://tinyurl.com/y878xmvk",
        "shortDescription": "Pokemon 12 Plush Gengar",
        "price": 19.9,
        "shippingPrice": 5
    },
    {
        "productId": 1,
        "code": "12356897452 ",
        "isActive": 1,
        "name": "LegoHP              ",
        "category": "Toys      ",
        "image": "https://m.media-amazon.com/images/I/91KjLP8wkxL._AC_SL1500_.jpg",
        "shortDescription": "LEGO Harry Potter Grindelwalds Escape",
        "price": 24.5,
        "shippingPrice": 2
    },
    {
        "productId": 11,
        "code": "2556256     ",
        "isActive": 1,
        "name": "Babolat Pure Aero   ",
        "category": "Equipment ",
        "image": "https://tinyurl.com/ybvt5qh9",
        "shortDescription": "Babolat PureAero detalji",
        "price": 218,
        "shippingPrice": 5
    },
    {
        "productId": 9,
        "code": "2556255     ",
        "isActive": 1,
        "name": "Grid Check Skirt    ",
        "category": "Clothes   ",
        "image": "https://tinyurl.com/y9xpvcmt",
        "shortDescription": "Grid Check Pinafore Skirt",
        "price": 18,
        "shippingPrice": 5
    },
]