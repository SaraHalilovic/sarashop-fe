import React from 'react'
import { QueryClient, QueryClientProvider, useQuery } from 'react-query';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import Navbar from './shared/components/Navbar';
import HomePage from './pages/HomePage';
import Order from './pages/Orders';
import Cart from './pages/Cart';
import ProductDetails from './pages/ProductDetails';


function App() {

  const queryClient = new QueryClient();
  return (
    <Router>
      <QueryClientProvider client={queryClient}>

        <div className="App">
          <Navbar />
          <Switch>
            <Route exact path="/">
              <HomePage />
            </Route>
            <Route path="/history">
              <Order />
            </Route>
            <Route path="/cart">
              <Cart />
            </Route>
            <Route path="/product/:id">
              <ProductDetails />
            </Route>
          </Switch>
        </div>
      </QueryClientProvider>
    </Router>
  );
}

export default App;
