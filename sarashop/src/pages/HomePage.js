import React from 'react'
import ProductCard from '../shared/components/ProductCard';
import ProductFilterPanel from '../shared/components/ProductsFilterPanel';
import { useQuery } from 'react-query'


function HomePage() {
    const[url, setUrl] = React.useState("https://localhost:44343/product")
    
    const fetchProducts = async ({ queryKey }) => {
        const response = await fetch(queryKey[1]).then(res => res.json())
        return response;
    }

    const { data, status } = useQuery(["products", url], fetchProducts, {
        keepPreviousData: true
    })

    if (status === "loading")
        return <div>Loading ...</div>

    if (status === "error")
        return <div>Error!</div>

    function handleReset() {
            setUrl("https://localhost:44343/product")
    }

    function handleSubmit(parametri){
        setUrl("https://localhost:44343/product?condition="+ parametri.condition 
        + "&name="+ parametri.keyword 
        + "&priceFrom="+ parametri.priceFrom 
        + "&priceTo="+ parametri.priceTo 
        + "&colour="+ parametri.colour
        + "&category="+ parametri.category
        + "&freeShipping="+ parametri.freeShipping)
    }
    return (

        <div className="product-page">
            <div className="page-container">
                <ProductFilterPanel handleSubmit={handleSubmit} handleReset={handleReset}/>
                <div className="card-group product-list">
                    {data.map(product => <ProductCard
                        key={product.productId}
                        id={product.productId}
                        name={product.name}
                        price={product.price}
                        image={product.image}
                        shortDescription={product.shortDescription}
                    
                    />)}
                </div>
            </div>
        </div>
    );
}

export default HomePage;