import React from 'react'
import { useQuery, useMutation } from 'react-query';
import CartItemCard from '../shared/components/CartItemCard';
import ProductsTable from '../shared/components/ProductsTable';

export default function Cart() {

    const [total, setTotal] = React.useState(false);

    const fetchCartItems = async () => {
        const cartItems = await fetch("https://localhost:44343/user/2/Cart")
            .then(res => res.json())
        return cartItems;
    }

    const deleteFromCart = async (productId) => {
        /* Treba dodati handlanje bad requesta */
        const response = await fetch(`https://localhost:44343/user/2/cart/cartitem/${productId}`, {
            method: "DELETE"
        })
        /*  .then(res => res.json()) Backend refactoring needed */
        return response
    }

    const { data, status, refetch } = useQuery("getCartItems", fetchCartItems, {
    })

    const { mutate } = useMutation(deleteFromCart, {
        onSuccess: () => {
            /* opicija, gdje se salje novi request i ceka update od servera - super slow (mutacija + refetch) => refactoring needed-*/
            refetch()

        }
    })

    if (status === "loading")
        return <div>Loading ...</div>

    if (status === "error")
        return <div>Error!</div>

    function handleDelete(productId) {
        mutate(parseInt(productId))
    }

    const cartList = data.map((item) =>
        <CartItemCard
            key={item.product.productId}
            id={item.product.productId}
            name={item.product.name}
            image={item.product.image}
            quantity={item.quantity}
            price={item.product.price}
            shippingPrice={item.product.price}
            handleDelete={handleDelete} />)

    const columns = ["Product", "Name", "Price", "Shipping Price", "Quantity", "", "Total"]

    return (
        <div>
            <h1 className="card-header">Cart</h1>
            <ProductsTable productList={cartList} columns={columns} />
            <button className="bg-dark checkout-btn" >Checkout</button>
        </div>
    )
}