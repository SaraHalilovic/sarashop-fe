import React from 'react'
import OrderCard from '../shared/components/OrderCard'
import { useQuery } from 'react-query'

export default function Orders() {

    const fetchOrderHistory = async () => {
        const response = fetch("https://localhost:44343/user/2/orderHistory").then(res => res.json())
        return response;
    }

    const { data, status } = useQuery("orderHistory", fetchOrderHistory)

    if (status === "loading")
        return <div>Loading ...</div>

    if (status === "error")
        return <div>Error!</div>

    const orderElements = data.map(order =>
        <OrderCard key={order.orderId}
            id={order.orderId}
            date={order.date}
            total={order.totalPrice} />)
    return (
        <>
            <h1 className="card-header">Order History</h1>
            <div className="orders-container">
                {orderElements}
            </div>
        </>
    )
}