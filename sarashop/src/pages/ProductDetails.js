import React from 'react'
import { useParams } from 'react-router'
import { useQuery, useMutation } from 'react-query'

export default function ProductDetails() {
    const [numberOfItems, setNumberOfItems] = React.useState(1);
    const [disable, setDisable] = React.useState(false);
    const { id } = useParams()

    const fetchProductDetails = async ({ queryKey }) => {
        const response = await fetch(`https://localhost:44343/product/Detailedproduct/id/${queryKey[1]}`)
            .then(res => res.json())
        return response;
        /* endpoint vraca objekat*/
    }

    const addToCart = async ( newProduct) => {
        /* Treaba dodati handlanje bad requesta */
        const response = await fetch(`https://localhost:44343/user/2/cart/CartItem`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(newProduct)
        })
            .then(res => res.json())
            setDisable(prev => !prev)
        return response
    }

    const { data, status } = useQuery(["productDetails", id], fetchProductDetails)
    const { mutate } = useMutation(addToCart, {})

    if (status === "loading")
        return <div>Loading ...</div>

    if (status === "error")
        return <div>Error!</div>

    function handleChange(event) {
        setNumberOfItems(event.target.value)
    }


    return (
        <div className="product-details-container">
            <div className="product-details--image-container">
                <img className="product-details--image" src={data.product.image} />
                <div >Shipping Price: {data.product.shippingPrice === 0 ? "Free" : `\$${data.product.shippingPrice}`}</div>
            </div>
            <div className="product-details--details">
                <table>
                    <tr className="product-details--row">
                        <td>Name:</td>
                        <td>{data.product.name}</td>
                    </tr>
                    <tr>
                        <td>Category:</td>
                        <td>{data.product.category}</td>
                    </tr>
                    <tr>
                        <td>Condition:</td>
                        <td>{data.productDetail.condition}</td>
                    </tr>
                    <tr>
                        <td>Color:</td>
                        <td>{data.productDetail.color}</td>
                    </tr>
                    <tr>
                        <td>Gender:</td>
                        <td>{data.productDetail.gender}</td>
                    </tr>
                    <tr>
                        <td>Sold:</td>
                        <td>{data.productDetail.itemsSold}</td>
                    </tr>
                    <tr>
                        <td>Available:</td>
                        <td>{data.productDetail.itemsInStore}</td>
                    </tr>
                    <tr>
                        <td>Price:</td>
                        <td>{data.product.price + data.product.shippingPrice}</td>
                    </tr>
                    <tr>
                        <td>Nmb</td>
                        <td> <input
                            id="product-filter-panel--search-keyword"
                            defaultValue={1}
                            onChange={handleChange} />
                            <br /></td>
                    </tr>
                    <tr>
                        <td>Total:</td>
                        <td>${numberOfItems * data.product.price + data.product.shippingPrice}</td>
                    </tr>
                    <tr>
                        <br />
                        <br />
                        <br />
                        <td>
                            <button className="btn btn-dark btn-lg"
                                disabled= {disable || data.productDetail.itemsInStore===0 || data.productDetail.itemsInStore < numberOfItems }
                                onClick={() => {
                                    mutate(
                                        {
                                            "productId":parseInt(id),
                                            "quantity": parseInt(numberOfItems)
                                        })
                                }}
                            >
                                Add to Cart</button></td>
                    </tr>
                </table>

            </div>
        </div>

    )
}